package factorial

import "testing"

func TestFactorial(t *testing.T) {
	t.Parallel()

	type args struct {
		input int
	}

	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "success",
			args: args{
				input: 5,
			},
			want: 120,
		},
	}

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if got := Factorial(tt.args.input); got != tt.want {
				t.Errorf("Factorial() = %v, want %v", got, tt.want)
			}
		})
	}
}
