package factorial

func Factorial(input int) int {
	if input <= 1 {
		return input
	}

	return input * Factorial(input-1)
}

func NewFactorial(input int) int {
	if input <= 1 {
		return input
	}

	return input * Factorial(input-1)
}
