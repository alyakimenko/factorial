# syntax=docker/dockerfile:1
# build stage
FROM golang:1.19.0-alpine3.16 as builder

# CI arguments
ARG CI_JOB_ID

# set a label
LABEL stage=builder \
    build_id=$CI_JOB_ID

# set workdir
WORKDIR /src/registration-service

# install dependencies
COPY go.mod go.sum ./
RUN go mod download

# copy source code
COPY . .

# version argument
ARG VERSION=0.0.0

# build a binary
RUN CGO_ENABLED=0 go build \
    -ldflags="-w -s -X gitlab.evopay.dev/financial-core/backend/integrations/registration-service/internal/config.Version=${VERSION}" \
    -o bin/registration-service cmd/registration-service/main.go

# final image
FROM alpine:3.16

# install ca-certificates and update ca-certificates
ARG CA_PKG_VERSION=20220614-r0
RUN apk update && apk --no-cache add ca-certificates=${CA_PKG_VERSION} && update-ca-certificates

# set nonroot user
ARG APP_USER=registration-service-user
RUN addgroup -g 2000 "$APP_USER" && adduser -g "" -D -u 1001 -G "$APP_USER" "$APP_USER"
USER "$APP_USER"

# set workdir
WORKDIR /opt/registration-service

# copy the binary from the builder stage
COPY --from=builder /src/registration-service/bin/registration-service registration-service

# set an executable
CMD [ "/opt/registration-service/registration-service" ]
